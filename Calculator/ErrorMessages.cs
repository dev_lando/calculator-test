﻿namespace Calculator
{
    public static class ErrorMessages
    {
        public const string invalidFilePath = "Invalid file path.";
        public const string invalidFileFormat = "Content of the provided file has not the correct structure.";
        public const string invalidOperation = "Provided symbol's operation is not valid. Allowed symbols are: +, -, *, /.";
        public const string invalidFirstArgument = "Provided FIRST argument is not a valid number.";
        public const string invalidSecondArgument = "Provided SECOND argument is not a valid number.";
        public const string secondArgumentIsZero = "SECOND argument can NOT be zero.";
    }
}
