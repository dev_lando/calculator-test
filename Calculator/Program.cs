﻿namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            string startingArgument = args.Length == 0 ? "" : args[0];

            IInputsProvider inputProvider = InputProviderGetter.GetInputprovider(startingArgument);
            var outputter = new Outputter();

            var userInputs = inputProvider.GetArguments();
            if (userInputs.Failed)
            {
                outputter.OutputErrorMessage(userInputs.ErrorMessage);
                return;
            }

            var validatedUserInputs = new InputValidatorAndParser().ValidateAndParseInputs(userInputs.Value);
            if (validatedUserInputs.Failed)
            {
                outputter.OutputErrorMessage(validatedUserInputs.ErrorMessage);
                return;
            }

            var result = new Calculator().PerformOperation(validatedUserInputs.Value);

            outputter.OutputTheResult(result);
        }
    }
}

