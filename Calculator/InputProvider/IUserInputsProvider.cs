﻿namespace Calculator
{
    public interface IInputsProvider
    {
        Result<(string firstArgument, string operationSymbol, string secondArgument)> GetArguments();
    }
}