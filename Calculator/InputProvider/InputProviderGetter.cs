﻿namespace Calculator
{
    public static class InputProviderGetter
    {
        public static IInputsProvider GetInputprovider(string startingArgument)
        {
            IInputsProvider inputProvider;

            if (string.IsNullOrWhiteSpace(startingArgument) || startingArgument == Mode.Interactive)
            {
                inputProvider = new CommandLineUserInputsProvider();
            }
            else
            {
                inputProvider = new FileInputProvider(startingArgument);
            }

            return inputProvider;
        }
    }
}
