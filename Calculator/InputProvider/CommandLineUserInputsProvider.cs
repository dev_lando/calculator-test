﻿using System;

namespace Calculator
{
    public class CommandLineUserInputsProvider : IInputsProvider
    {
        public Result<(string firstArgument, string operationSymbol, string secondArgument)> GetArguments()
        {
            Console.WriteLine("First argument: ");
            var firstArgument = Console.ReadLine();

            Console.WriteLine("Operation symbol (+, -, *, /): ");
            var operationSymbol = Console.ReadLine();

            Console.WriteLine("Second Argument: ");
            var secondArgument = Console.ReadLine();

            return Result<(string firstArgument, string operationSymbol, string secondArgument)>.Success((firstArgument: firstArgument, operationSymbol: operationSymbol, secondArgument: secondArgument));
        }
    }
}
