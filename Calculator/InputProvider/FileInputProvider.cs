﻿using System;
using System.Linq;

namespace Calculator
{
    public class FileInputProvider : IInputsProvider
    {
        private string Path;

        public FileInputProvider(string path)
        {
            this.Path = path;
        }

        public Result<(string firstArgument, string operationSymbol, string secondArgument)> GetArguments()
        {
            if (!System.IO.File.Exists(this.Path))
            {
                return Result<(string firstArgument, string operationSymbol, string secondArgument)>.Failure(ErrorMessages.invalidFilePath);
            }

            var readAllText = System.IO.File.ReadAllText(this.Path);
            var linesContent = readAllText.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries)
                                .Select(line => line.Trim()).ToArray();

            if (!FileContentHasTheRightStructure(linesContent))
            {
                return Result<(string firstArgument, string operationSymbol, string secondArgument)>.Failure(ErrorMessages.invalidFileFormat);
            }

            return Result<(string firstArgument, string operationSymbol, string secondArgument)>.Success((firstArgument: linesContent[0], operationSymbol: linesContent[1], secondArgument: linesContent[2]));
        }

        private bool FileContentHasTheRightStructure(string[] linesContent)
        {
            return linesContent.Where(line => !String.IsNullOrEmpty(line)).Count() == 3;
        }
    }
}
