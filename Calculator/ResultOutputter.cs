﻿using System;

namespace Calculator
{
    public class Outputter
    {
        public void OutputTheResult(double result)
        {
            Console.WriteLine("Result: " + result);
            Console.ReadLine();
        }

        public void OutputErrorMessage(string error)
        {
            Console.WriteLine();
            Console.WriteLine("WARNING");
            Console.WriteLine(error);
            Console.ReadLine();
        }
    }
}
