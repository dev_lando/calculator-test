﻿using System;

namespace Calculator
{
    public class Calculator
    {

        private static Func<int, int, double> sum = (int arg1, int arg2) => { return arg1 + arg2; };
        private static Func<int, int, double> subtraction = (int arg1, int arg2) => { return arg1 - arg2; };
        private static Func<int, int, double> multiplication = (int arg1, int arg2) => { return arg1 * arg2; };
        private static Func<int, int, double> division = (int arg1, int arg2) => { return arg1 / (double)arg2; };

        private Func<int, int, double>[] operationsPerformer = new Func<int, int, double>[] {
            sum,
            subtraction,
            multiplication,
            division
        };

        public double PerformOperation(CalculatorInputs inputs)
        {
            return operationsPerformer[(int)inputs.Operation](inputs.FirstArgument, inputs.SecondArgument);
        }
    }
}
