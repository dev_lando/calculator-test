﻿using System;
using System.Collections.Generic;

namespace Calculator
{
    public class InputValidatorAndParser
    {
        private Dictionary<string, SupportedOperations> SymbolToOperationMap = new Dictionary<string, SupportedOperations>()
        {
            {"+", SupportedOperations.Sum},
            {"-", SupportedOperations.Subtraction},
            {"*", SupportedOperations.Multiplication},
            {"/", SupportedOperations.Division}
        };

        public Result<CalculatorInputs> ValidateAndParseInputs((string firstArgument, string operationSymbol, string secondArgument) userInputs)
        {
            // validate and parse FIRST operand
            var validateAndParseFirstOperandResult = ValidateAndParseOperand(userInputs.firstArgument);
            if (!validateAndParseFirstOperandResult.Item1)
            {
                return Result<CalculatorInputs>.Failure(ErrorMessages.invalidFirstArgument);
            }
            var firstArgument = validateAndParseFirstOperandResult.Item2;

            // validate and parse OPERATION
            var operationSymbol = userInputs.operationSymbol.Trim();
            if (!OperationSymbolIsValid(operationSymbol))
            {
                return Result<CalculatorInputs>.Failure(ErrorMessages.invalidOperation);
            }
            var operation = SymbolToOperationMap[operationSymbol];

            // validate and parse SECOND operand
            var validateAndParseSecondOperandResult = ValidateAndParseOperand(userInputs.secondArgument);
            if (!validateAndParseSecondOperandResult.Item1)
            {
                return Result<CalculatorInputs>.Failure(ErrorMessages.invalidSecondArgument);
            }
            if (operation == SupportedOperations.Division && validateAndParseSecondOperandResult.Item2 == 0)
            {
                return Result<CalculatorInputs>.Failure(ErrorMessages.secondArgumentIsZero);
            }
            var secondArgument = validateAndParseSecondOperandResult.Item2;

            return Result<CalculatorInputs>.Success(new CalculatorInputs(firstArgument, secondArgument, operation));
        }

        private bool OperationSymbolIsValid(string operationSymbol)
        {
            return this.SymbolToOperationMap.ContainsKey(operationSymbol);
        }

        private Tuple<bool, int> ValidateAndParseOperand(string operand)
        {
            var isNumeric = int.TryParse(operand, out int operandValue);
            return new Tuple<bool, int>(isNumeric, operandValue);
        }
    }
}
