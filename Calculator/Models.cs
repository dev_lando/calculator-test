﻿namespace Calculator
{
    public static class Mode
    {
        public const string Interactive = "interactive";
    }

    public enum SupportedOperations
    {
        Sum = 0,
        Subtraction = 1,
        Multiplication = 2,
        Division = 3
    };

    public class CalculatorInputs
    {
        public int FirstArgument { get; private set; }
        public int SecondArgument { get; private set; }
        public SupportedOperations Operation { get; private set; }

        public CalculatorInputs(int firstArg, int secondArg, SupportedOperations operation)
        {
            this.FirstArgument = firstArg;
            this.SecondArgument = secondArg;
            this.Operation = operation;
        }
    }

    public class Result<T>
    {
        public string ErrorMessage { get; private set; }

        public bool Failed
        {
            get
            {
                return !string.IsNullOrEmpty(ErrorMessage);
            }
        }

        public T Value { get; private set; }


        public static Result<T> Success(T value)
        {
            return new Result<T>
            {
                ErrorMessage = string.Empty,
                Value = value
            };
        }

        public static Result<T> Failure(string errorMessage)
        {
            return new Result<T>
            {
                ErrorMessage = errorMessage
            };
        }

    }
}
