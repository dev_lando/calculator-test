﻿using System.Collections.Generic;
using Xunit;

namespace Calculator
{
    public class InputValidatorAndParserTest
    {

        public static IEnumerable<object[]> ValidateAndParseInputsTestData => new List<object[]>
        {
            new object[] { "a", "+", "7", Result<CalculatorInputs>.Failure(ErrorMessages.invalidFirstArgument) },
            new object[] { "", "+", "7", Result<CalculatorInputs>.Failure(ErrorMessages.invalidFirstArgument) },
            new object[] { "7", "+", "a", Result<CalculatorInputs>.Failure(ErrorMessages.invalidSecondArgument) },
            new object[] { "7", "+", "", Result<CalculatorInputs>.Failure(ErrorMessages.invalidSecondArgument) },
            new object[] { "7", "a", "9", Result<CalculatorInputs>.Failure(ErrorMessages.invalidOperation) },
            new object[] { "7", "", "9", Result<CalculatorInputs>.Failure(ErrorMessages.invalidOperation) },
            new object[] { "7", "/", "0", Result<CalculatorInputs>.Failure(ErrorMessages.secondArgumentIsZero) },
            new object[] { "7", "+", "9", Result<CalculatorInputs>.Success(new CalculatorInputs(7,9,SupportedOperations.Sum)) },
            new object[] { "  7   ", "    +   ", "   9    ", Result<CalculatorInputs>.Success(new CalculatorInputs(7, 9, SupportedOperations.Sum)) }
        };

        [Theory]
        [MemberData(nameof(ValidateAndParseInputsTestData))]
        public void ValidateAndParseInputsTest(string firstArg, string operationSymbol, string secondArg, Result<CalculatorInputs> expectedResult)
        {
            // Arrange
            var validatorAndParser = new InputValidatorAndParser();
            var userInput = (firstArg, operationSymbol, secondArg);

            // Act
            var result = validatorAndParser.ValidateAndParseInputs(userInput);

            // Assert
            Assert.Equal(expectedResult.ErrorMessage, result.ErrorMessage);
            if (!expectedResult.Failed)
            {
                Assert.Equal(expectedResult.Value.FirstArgument, result.Value.FirstArgument);
                Assert.Equal(expectedResult.Value.SecondArgument, result.Value.SecondArgument);
                Assert.Equal(expectedResult.Value.Operation, result.Value.Operation);
            }
        }
    }
}
