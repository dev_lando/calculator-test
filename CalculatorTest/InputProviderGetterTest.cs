﻿using Xunit;

namespace Calculator
{
    public class InputProviderGetterTest
    {

        [Theory]
        [InlineData(Mode.Interactive, typeof(CommandLineUserInputsProvider))]
        [InlineData("", typeof(CommandLineUserInputsProvider))]
        [InlineData(" ", typeof(CommandLineUserInputsProvider))]
        [InlineData("file path", typeof(FileInputProvider))]
        public void GetInputproviderTest(string startingArgument, System.Type expectedType)
        {
            // Act
            var result = InputProviderGetter.GetInputprovider(startingArgument);

            // Assert
            Assert.IsType(expectedType, result);
        }
    }
}
