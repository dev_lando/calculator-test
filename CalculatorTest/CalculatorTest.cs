using Xunit;

namespace Calculator
{
    public class CalculatorTest
    {
        [Theory]
        [InlineData(4, SupportedOperations.Sum, 3, 7)]
        [InlineData(4, SupportedOperations.Subtraction, 3, 1)]
        [InlineData(4, SupportedOperations.Multiplication, 3, 12)]
        [InlineData(5, SupportedOperations.Division, 2, 2.5)]
        public void PerformOperationTest(int firstArg, SupportedOperations operation, int secondArg, double expectedResult)
        {
            // Arrange
            var calculator = new Calculator();
            var calculatorInputs = new CalculatorInputs(firstArg, secondArg, operation);

            // Act
            var result = calculator.PerformOperation(calculatorInputs);

            // Assert
            Assert.Equal(expectedResult, result);
        }
    }
}
