﻿using System.Collections.Generic;
using Xunit;

namespace Calculator
{
    public class FileInputProviderTest
    {
        const string InputFileFolderName = "InputFileTest";

        public static IEnumerable<object[]> GetArgumentsTestData => new List<object[]>
        {
            new object[] { "input.txt", Result<(string firstArgument, string operationSymbol, string secondArgument)>.Success(("10", "+", "12"))},
            new object[] { "invalid file path", Result<(string firstArgument, string operationSymbol, string secondArgument)>.Failure(ErrorMessages.invalidFilePath)},
            new object[] { "invalid1.txt", Result<(string firstArgument, string operationSymbol, string secondArgument)>.Failure(ErrorMessages.invalidFileFormat) },
            new object[] { "invalid2.txt", Result<(string firstArgument, string operationSymbol, string secondArgument)>.Failure(ErrorMessages.invalidFileFormat) },
            new object[] { "invalid3.txt", Result<(string firstArgument, string operationSymbol, string secondArgument)>.Failure(ErrorMessages.invalidFileFormat) },
        };

        [Theory]
        [MemberData(nameof(GetArgumentsTestData))]
        public void GetArgumentsTest(string fileName, Result<(string firstArgument, string operationSymbol, string secondArgument)> expected)
        {
            // Arrange
            var fileInputProvider = new FileInputProvider(InputFileFolderName + "\\" + fileName);

            // Act
            var result = fileInputProvider.GetArguments();

            // Assert
            if (result.Failed)
            {
                Assert.Equal(expected.ErrorMessage, result.ErrorMessage);
                return;
            }

            var valueResult = result.Value;
            var valueExpected = expected.Value;
            Assert.Equal(valueExpected.firstArgument, valueResult.firstArgument);
            Assert.Equal(valueExpected.secondArgument, valueResult.secondArgument);
            Assert.Equal(valueExpected.operationSymbol, valueResult.operationSymbol);
        }
    }
}




